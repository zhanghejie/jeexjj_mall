/****************************************************
 * Description: Service for t_mall_shiro_filter
 * Copyright:   Copyright (c) 2018
 * Company:     xjj
 * @author      zhanghejie
 * @version     1.0
 * @see
	HISTORY
    *  2018-09-13 zhanghejie Create File
**************************************************/
package com.xjj.mall.service;
import com.xjj.mall.entity.ShiroFilterEntity;
import com.xjj.framework.service.XjjService;

public interface ShiroFilterService  extends XjjService<ShiroFilterEntity>{
	

}
